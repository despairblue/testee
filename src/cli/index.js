/* @flow */
'use strict'

import run from './commands/run.js'
import test from './commands/test.js'
import {
  errors,
  filter,
  readPackageJson
} from './check-pkg-json.js'

const commander = require('commander')
const invariant = require('invariant')
const pkg = require('../../package.json')
const util = require('util')
const debug = require('debug')('testee:cli')

// get global options
commander.version(pkg.version)
commander.usage('[command] [flags]')
commander.option('--test-regex', 'what files are considered test files')
commander.option('--source-regex', 'what files are considered source files')

// WATCH
commander
  .command('watch')
  .description('start watching')
  .action(function () {
    const packageJSON = readPackageJson(process.cwd())

    invariant(packageJSON !== false, 'There is either no package.json in this directory or it contains no valid JSON')

    const packageJSONErrors = errors(packageJSON)

    invariant(packageJSONErrors === false, util.format('package.json does not contain a valid testee-configuration: %j', packageJSONErrors))

    const configuration = filter(packageJSON)

    run({
      directory: process.cwd(),
      entryPoint: configuration.main,
      testGlobs: configuration.testee.testGlobs,
      script: configuration.testee.script
    })
  })

// TEST
commander
  .command('test [commit-ish]')
  .description('run tests since this commit')
  .action(function (commitish) {
    debug('commit-ish: %s', commitish)

    const packageJSON = readPackageJson(process.cwd())

    invariant(packageJSON !== false, 'There is either no package.json in this directory or it contains no valid JSON')

    const packageJSONErrors = errors(packageJSON)

    invariant(packageJSONErrors === false, util.format('package.json does not contain a valid testee-configuration: %j', packageJSONErrors))

    const configuration = filter(packageJSON)

    test({
      directory: process.cwd(),
      entryPoint: configuration.main,
      testGlobs: configuration.testee.testGlobs,
      script: configuration.testee.script,
      hash: commitish
    })
  })

commander.parse(process.argv)
