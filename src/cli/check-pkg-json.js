/* @flow */
'use strict'

const path = require('path')
const fs = require('fs')
const validator = require('is-my-json-valid')

type Glob = {
  include: string,
  exclude?: string
}

export type Configuration = {
  main: string,
  testee: {
    testGlobs: Array<Glob>,
    script: string
  }
}

export type VerboseError = {
  field: string,
  message: string,
  value: any,
  type: string
}

const schema = {
  required: true,
  type: 'object',
  properties: {
    main: {
      required: true,
      type: 'string'
    },
    testee: {
      required: true,
      type: 'object',
      properties: {
        testGlobs: {
          required: true,
          type: 'array',
          items: {
            type: 'object',
            properties: {
              include: {
                required: true,
                type: 'string'
              },
              exclude: {
                type: 'string'
              }
            }
          }
        },
        script: {
          required: true,
          type: 'string'
        }
      }
    }
  }
}

const filterSchema = {
  ...schema,
  additionalProperties: false
}

const jsonValidate = validator(schema, { verbose: true })
const jsonFilter = validator.filter(filterSchema)

export function errors (object: Object): VerboseError | false {
  jsonValidate(object)

  if (jsonValidate.errors) {
    return jsonValidate.errors
  } else {
    return false
  }
}

export function filter (object: Object): Configuration {
  return jsonFilter(object)
}

export function readPackageJson (directory: string): Object | false {
  try {
    return JSON.parse(
      fs.readFileSync(path.join(directory, 'package.json'), 'utf8')
    )
  } catch (error) {
    return false
  }
}
