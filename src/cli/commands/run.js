/* @flow */
'use strict'

import elmInterop from '../../elm-interop.js'
import getDependencyTree from '../../get-dependency-tree.js'

const _ = require('lodash')
const spawn = require('cross-spawn')
const chokidar = require('chokidar')
const debug = require('debug')('testee:run')
const glob = require('glob')
const path = require('path')

type Glob = {
  include: string,
  exclude?: string
}

export type RunOptions = {
  directory: string,
  entryPoint: string,
  testGlobs: Array<Glob>,
  script: string
}

export default async function run ({
  directory = process.cwd(),
  testGlobs = [],
  entryPoint,
  script
}: RunOptions) {
  debug('Parsing project.')
  const tree = getDependencyTree(entryPoint)
  const testFiles: Array<string> =
    _.flatMap(testGlobs, function (patterns): Array<string> {
      let paths = glob
        .sync(path.join(directory, patterns.include))

      if (patterns.exclude) {
        paths = paths.filter((name) => !name.includes(patterns.exclude))
      }

      return paths
    })
  const testTrees = testFiles.map(getDependencyTree)

  debug('Waiting for elm.')
  await Promise.all([
    elmInterop.nodeIn(tree),
    ...testTrees.map(elmInterop.testIn)
  ])
  debug('Elm is done')

  process.on('SIGUSR2', function () {
    elmInterop.logState()
  })

  chokidar.watch(directory, {
    ignored: /[/\\]\./
  }).on('change', async function (path_: string) {
    const relativeFilePath = path.relative(directory, path_)
    const files = _.map(
      await elmInterop.getTestsForFile(
        path.resolve(directory, relativeFilePath)
      ),
      _.partial(path.relative, directory)
    )
    debug('%s changed. Tests to run are: %j', relativeFilePath, files)
    if (files.length) {
      const cmd = `${script} ${files.join(' ')}`

      debug('spawning "%s" in bash', cmd)

      const result = spawn.sync('bash', [ '-c', cmd ], { stdio: 'inherit' })

      if (result.error) {
        console.error(result.error)
      }
    }
  })
}
