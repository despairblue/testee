/* @flow */
'use strict'

import elmInterop from '../../elm-interop.js'
import getDependencyTree from '../../get-dependency-tree.js'

const _ = require('lodash')
const childProcess = require('child_process')
const spawn = require('cross-spawn')
const debug = require('debug')('testee:test')
const glob = require('glob')
const path = require('path')

type Glob = {
  include: string,
  exclude?: string
}

type Configuration = {
  directory: string,
  testGlobs: Array<Glob>,
  entryPoint: string,
  script: string,
  hash: string
}

const setupElmInterop = function (
  entryPoint: string,
  testGlobs: Array<Glob>,
  directory: string
): Promise<void> {
  const tree = getDependencyTree(entryPoint)
  const testFiles: Array<string> =
    _.flatMap(testGlobs, function (patterns): Array<string> {
      let paths = glob
        .sync(path.join(directory, patterns.include))

      if (patterns.exclude) {
        paths = paths.filter((name) => !name.includes(patterns.exclude))
      }

      return paths
    })
  const testTrees = testFiles.map(getDependencyTree)

  return Promise.all([
    elmInterop.nodeIn(tree),
    ...testTrees.map(elmInterop.testIn)
  ]).then(() => {})
}

export default async function test ({
  directory,
  testGlobs,
  entryPoint,
  script,
  hash
}: Configuration): Promise<void> {
  const currentRevision = (await new Promise(function (resolve, reject) {
    childProcess.exec('git rev-parse HEAD', function (
      error: string,
      stdout: string
    ) {
      if (error) {
        reject(error)
      } else {
        resolve(stdout)
      }
    })
  })).trim()
  const files = await new Promise(function (resolve, reject) {
    childProcess.exec(`git diff --name-only ${hash} ${currentRevision}`, function (
      error: string,
      stdout: string
    ) {
      if (error) {
        reject(error)
      } else {
        const absoluteFiles = stdout
          .split('\n')
          .map(function (file) {
            return path.resolve(directory, file)
          })
        resolve(absoluteFiles)
      }
    })
  })
  debug('Current revision of HEAD is %s', currentRevision)
  debug('Changed files are %j', files)
  debug('Waiting for elm.')
  await setupElmInterop(entryPoint, testGlobs, directory)
  debug('Elm is done.')

  const testFiles = _.flow(
    _.flatten,
    _.uniq
  )(
    await Promise.all(files.map(function (file) {
      return elmInterop.getTestsForFile(file)
    }))
  )
  const cmd = `${script} ${testFiles.join(' ')}`

  debug('Spawning "%s" in bash', cmd)

  const result = spawn.sync('bash', [ '-c', cmd ], { stdio: 'inherit' })

  if (result.error) {
    console.error(result.error)
  }
}
