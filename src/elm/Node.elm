module Node exposing (newNode, getPath, nodeDecoder, collectDependents, collectTestDependents, Node, combineAll)

import Json.Decode as Json exposing (..)
import Json.Decode.Extra exposing (lazy)
import Dict exposing (Dict)
import List.Extra exposing (dropDuplicates)


type Node
    = Node
        { path : String
        , nodes : List Node
        }


newNode : String -> List Node -> Node
newNode path nodes =
    Node
        { path = path
        , nodes = nodes
        }


getPath : Node -> String
getPath (Node { path }) =
    path


nodeDecoder : Decoder Node
nodeDecoder =
    object2 newNode
        ("path" := string)
        ("nodes" := list (lazy (\_ -> nodeDecoder)))


combineAll : List (Dict String (List String)) -> Dict String (List String)
combineAll dicts =
    let
        existsInBoth key leftValue rightValue dict =
            Dict.insert key (dropDuplicates <| List.append rightValue leftValue) dict

        merge dict1 dict2 =
            Dict.merge Dict.insert existsInBoth Dict.insert dict1 dict2 Dict.empty
    in
        case dicts of
            [] ->
                Dict.empty

            [ dict ] ->
                dict

            dict :: t ->
                List.foldr merge dict t


collectDependents_ : List String -> Node -> Dict String (List String)
collectDependents_ dependents (Node { path, nodes }) =
    case nodes of
        [] ->
            Dict.singleton path dependents

        _ ->
            let
                newDependents =
                    path :: dependents
            in
                nodes
                    |> List.map (collectDependents_ newDependents)
                    |> (::) (Dict.singleton path dependents)
                    |> combineAll



-- |
-- >>> collectDependents (newNode "a" [(newNode "b" [newNode "c" []]), (newNode "c" [])])
-- Dict.fromList [("a", []), ("b", ["a"]), ("c", ["a", "b"])]


collectDependents : Node -> Dict String (List String)
collectDependents node =
    collectDependents_ [] node


collectTestDependents_ : String -> Node -> Dict String (List String)
collectTestDependents_ testPath (Node { path, nodes }) =
    case nodes of
        [] ->
            (Dict.singleton path [ testPath ])

        _ ->
            nodes
                |> List.map (collectTestDependents_ testPath)
                |> (::) (Dict.singleton path [ testPath ])
                |> combineAll



-- |
-- >>> collectTestDependents (newNode "t" [(newNode "a" [newNode "b" []]), (newNode "b" [])])
-- Dict.fromList [("a", ["t"]), ("b", ["t"]), ("t", ["t"])]


collectTestDependents : Node -> Dict String (List String)
collectTestDependents (Node { path, nodes }) =
    nodes
        |> List.map (collectTestDependents_ path)
        |> (::) (Dict.singleton path [ path ])
        |> combineAll
