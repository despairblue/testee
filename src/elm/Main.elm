port module Main exposing (..)

import Worker
import Json.Decode as Json exposing (..)
import Debug exposing (log)
import Dict exposing (Dict)
import Node exposing (Node)


-- MODEL


type alias Model =
    { dependencyTree : Node
    , dependentsDict : Dict String (List String)
    , testDependents : Dict String (List String)
    }


init : ( Model, Cmd Msg )
init =
    Model (Node.newNode "" []) Dict.empty Dict.empty ! []


type alias Tag =
    String


type Msg
    = NoOp
    | InputNode ( Tag, Node )
    | InputTest ( Tag, Node )
    | GetTestsFor ( Tag, String )
    | LogState



-- PORTS


port nodeIn : (( Tag, Json.Value ) -> msg) -> Sub msg


port nodeInDone : Tag -> Cmd msg


port testIn : (( Tag, Json.Value ) -> msg) -> Sub msg


port testInDone : Tag -> Cmd msg


port getTestsForFile : (( String, String ) -> msg) -> Sub msg


port getDependencyTree : List ( String, List String ) -> Cmd msg


port returnTests : ( Tag, List String ) -> Cmd msg


port errorOut : String -> Cmd msg


port logState : (Maybe String -> msg) -> Sub msg


modelOut : Model -> Cmd msg
modelOut model =
    model.testDependents
        |> Dict.toList
        |> getDependencyTree



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        InputNode ( tag, node ) ->
            { model
                | dependencyTree = node
                , dependentsDict = Node.collectDependents node
            }
                ! [ nodeInDone tag ]

        InputTest ( tag, node ) ->
            { model
                | testDependents =
                    Node.combineAll [ model.testDependents, Node.collectTestDependents node ]
            }
                ! [ testInDone tag ]

        GetTestsFor tuple ->
            model ! [ (getTestsFor model tuple) |> returnTests ]

        LogState ->
            log "State" model ! []

        NoOp ->
            model ! []


getFiles : Dict String (List String) -> String -> List String
getFiles dict file =
    dict |> Dict.get file |> Maybe.withDefault []


getTestsFor : Model -> ( Tag, String ) -> ( Tag, List String )
getTestsFor { dependentsDict, testDependents } ( tag, file ) =
    let
        concernedFiles =
            file
                |> getFiles dependentsDict
                |> (::) file
                |> log "concernedFiles"

        concernedTests =
            concernedFiles
                |> List.map (getFiles testDependents)
                |> List.foldr List.append []
                |> log "concernedTests"
    in
        ( tag, concernedTests )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    let
        parseNode ( tag, node ) =
            case node of
                Ok node ->
                    InputNode ( tag, node )

                Err msg ->
                    NoOp

        parseTest ( tag, node ) =
            case node of
                Ok node ->
                    InputTest ( tag, node )

                Err msg ->
                    NoOp
    in
        Sub.batch
            [ nodeIn (\( tag, value ) -> ( tag, decodeValue Node.nodeDecoder value ))
                |> Sub.map parseNode
            , testIn (\( tag, value ) -> ( tag, decodeValue Node.nodeDecoder value ))
                |> Sub.map parseTest
            , getTestsForFile GetTestsFor
            , logState (\_ -> LogState)
            ]



-- MAIN


main : Program Never
main =
    Worker.program modelOut
        { init = init
        , update = update
        , subscriptions = subscriptions
        }
