/* @flow */
'use strict'

import type { $DependencyTree } from 'dependency-tree'

const dependencyTree = require('dependency-tree')
const path = require('path')
const _ = require('lodash')

export type ProperTree = {
  path: string,
  nodes: ProperTrees
}

type ProperTrees = Array<ProperTree>

const toProperTree_ = function (tree : $DependencyTree): ProperTrees {
  return _.map(tree, function (childNode: $DependencyTree, path: string) {
    return {
      path,
      nodes: toProperTree_(childNode)
    }
  })
}

const toProperTree = function (tree: $DependencyTree) : ProperTree {
  const path: string = _.flow(
    _.keys,
    _.head
  )(tree)

  return {
    path,
    nodes: toProperTree_(tree[path])
  }
}

const cache = {}

export default function (filename: string) : ProperTree {
  const absFilename = path.resolve(process.cwd(), filename)
  const directory = path.dirname(absFilename)
  const depTree = dependencyTree({
    filename: absFilename,
    directory,
    visited: cache,
    filter: (path) => path.indexOf('node_modules') === -1
  })

  return toProperTree(depTree)
}
