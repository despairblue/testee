/* @flow */
'use strict'

import type { ProperTree } from './get-dependency-tree.js'

/* eslint-disable require-path-exists/exists */
/* $FlowFixMe - module only exists after build in lib folger */
const Elm: ElmType = require('./elm/Elm')
// import Elm from '../../elm/Elm'
/* eslint-enable require-path-exists/exists */

const shortid = require('shortid')

export type Interop = {
  nodeIn: (tree: ProperTree) => Promise<void>,
  testIn: (tree: ProperTree) => Promise<void>,
  getTestsForFile: (file: string) => Promise<Array<string>>
}

type Worker = {
  ports: {
    nodeIn: {
      send: (tuple: [string, ProperTree]) => void
    },
    nodeInDone: {
      subscribe: (cb: (tag: string) => void) => void,
      unsubscribe: (cb: (tag: string) => void) => void
    },
    testIn: {
      send: (tuple: [string, ProperTree]) => void
    },
    testInDone: {
      subscribe: (cb: (tag: string) => void) => void,
      unsubscribe: (cb: (tag: string) => void) => void
    },
    returnTests: {
      subscribe: (cb: (tuple: [ string, Array<string> ]) => void) => void,
      unsubscribe: (cb: (tuple: [ string, Array<string> ]) => void) => void
    },
    getTestsForFile: {
      send: (tagNFileName: [string, string]) => void
    },
    logState: {
      send: () => void
    }
  }
}

type ElmType = {
  Main: {
    worker: () => Worker
  }
}

let worker: Worker

const init = function (): Worker {
  if (!worker) {
    worker = Elm.Main.worker()
  }

  return worker
}

export default {
  nodeIn: function (tree: ProperTree): Promise<void> {
    return new Promise(function (resolve) {
      const worker = init()
      const tag = shortid()
      const cb = function (tag_) {
        if (tag_ === tag) {
          worker.ports.nodeInDone.unsubscribe(cb)
          resolve()
        }
      }

      worker.ports.nodeInDone.subscribe(cb)
      worker.ports.nodeIn.send([ tag, tree ])
    })
  },
  testIn: function (tree: ProperTree): Promise<void> {
    return new Promise(function (resolve) {
      const worker = init()
      const tag = shortid()
      const cb = function (tag_) {
        if (tag_ === tag) {
          worker.ports.testInDone.unsubscribe(cb)
          resolve()
        }
      }

      worker.ports.testInDone.subscribe(cb)
      worker.ports.testIn.send([ tag, tree ])
    })
  },
  getTestsForFile: function (file: string): Promise<Array<string>> {
    return new Promise(function (resolve) {
      const worker = init()
      const tag = shortid()
      const cb = function ([ tag_: string, testPaths: Array<string> ]) {
        if (tag_ === tag) {
          worker.ports.returnTests.unsubscribe(cb)

          resolve(testPaths)
        }
      }

      worker.ports.returnTests.subscribe(cb)
      worker.ports.getTestsForFile.send([ tag, file ])
    })
  },
  logState: function () {
    worker.ports.logState.send(null)
  }
}
