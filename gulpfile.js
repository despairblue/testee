'use strict'

const babel = require('gulp-babel')
const fs = require('fs')
const gulp = require('gulp')
const newer = require('gulp-newer')
const path = require('path')
const gutil = require('gulp-util')
const watch = require('gulp-watch')
const plumber = require('gulp-plumber')
const elm = require('gulp-elm')

const babelRc = JSON.parse(fs.readFileSync(path.join(__dirname, '.babelrc'), 'utf8'))

gulp.task('default', [ 'build' ])

gulp.task('build', [ 'build-js', 'build-elm' ])

gulp.task('build-js', function () {
  return gulp.src('src/**/*.js')
  .pipe(plumber({
    errorHandler (err) {
      gutil.log(err.stack)
    }
  }))
  .pipe(newer('lib'))
  .pipe(babel(babelRc.env.node5))
  .pipe(gulp.dest('lib'))
})

gulp.task('elm-init', elm.init)

gulp.task('build-elm', [ 'elm-init' ], function () {
  return gulp.src('src/**/*.elm')
    .pipe(plumber({
      errorHandler (error) {
        gutil.log(error.stack)
      }
    }))
    .pipe(newer('lib/elm'))
    .pipe(elm.bundle('Elm.js'))
    .pipe(gulp.dest('lib/elm'))
})

gulp.task('watch', [ 'watch-js', 'watch-elm' ])

gulp.task('watch-js', [ 'build' ], function () {
  watch('src/**/*.js', function () {
    gulp.start('build-js')
  })
})

gulp.task('watch-elm', [ 'build' ], function () {
  watch('src/**/*.elm', function () {
    gulp.start('build-elm')
  })
})
