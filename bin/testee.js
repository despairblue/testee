#!/usr/bin/env node
'use strict'

const debug = require('debug')('testee:bin')

try {
  require('../lib/cli')
} catch (error) {
  console.error(error.message)

  debug(error.stack)

  process.exit(1)
}
